import time
import sys
import os
from selenium import webdriver
from selenium.webdriver.common.by import By
options = webdriver.FirefoxOptions()
options.headless = True 
driver = webdriver.Firefox(options=options)


def cls():
    os.system('cls' if os.name == 'nt' else 'clear')


def colored(r, g, b, text):
    return "\033[38;2;{};{};{}m{} \033[38;2;255;255;255m".format(r, g, b, text)


def lkpr():
    driver.get('https://meteo.rlp.cz/xLKPR_metrep.htm?')
    time.sleep(2)
    lkpr = "Airport Praha - Ruzyně, Lenght 3220 m."
    rwy06 = "Runway 6"
    rwy24 = "Runway 24"
    ga1 = 110
    ga2 = 20
    ga3 = 300
    ga4 = 200
    windir24 = driver.find_element(By.XPATH,'/html/body/table[2]/tbody/tr[3]/th[2]').text
    windir24 = int(windir24)
    windspeed24 = driver.find_element(By.XPATH,'/html/body/table[2]/tbody/tr[3]/th[3]').text
    windspeed24 = int(windspeed24)
    visibilitylkpr = driver.find_element(By.XPATH,'/html/body/table[1]/tbody/tr[3]/td[2]').text
    visibilitylkpr = int(visibilitylkpr)
    rules(windir24,windspeed24,visibilitylkpr,lkpr,rwy06,rwy24,ga1,ga2,ga3,ga4)


def lkkv():
    driver.get('https://meteo.rlp.cz/xLKKV_metrep.htm?')
    time.sleep(2)
    lkkv = "Airport Karlovy Vary, Lenght 2150 m."
    rwy11 = "Runway 11"
    rwy29 = "Runway 29"
    ga5 = 150
    ga6 = 70
    ga7 = 330
    ga8 = 250
    windir29 = driver.find_element(By.XPATH,'/html/body/table[2]/tbody/tr[3]/th[2]/big').text
    windir29 = int(windir29)
    windspeed29 = driver.find_element(By.XPATH,'/html/body/table[2]/tbody/tr[3]/th[3]/big').text
    windspeed29 = int(windspeed29)
    visibilitylkkv = driver.find_element(By.XPATH,'/html/body/table[1]/tbody/tr[3]/td[2]').text
    visibilitylkkv = int(visibilitylkkv)
    rules(windir29,windspeed29,visibilitylkkv,lkkv,rwy11,rwy29,ga5,ga6,ga7,ga8)


def lkmt():
    driver.get('https://meteo.rlp.cz/xLKMT_metrep.htm?')
    time.sleep(2)
    lkmt = "Airport Ostrava - Mošnov, Lenght 3500 m"
    rwy04 = "Runway 4"
    rwy22 = "Runway 22"
    ga9 = 80
    ga10 = 0
    ga11 = 260
    ga12 = 180
    windir04 = driver.find_element(By.XPATH,'/html/body/table[2]/tbody/tr[3]/th[2]/big').text
    windir04 = int(windir04)
    windspeed04 = driver.find_element(By.XPATH,'/html/body/table[2]/tbody/tr[3]/th[3]/big').text
    windspeed04 = int(windspeed04)
    visibilitylkmt = driver.find_element(By.XPATH,'/html/body/table[1]/tbody/tr[3]/td[2]').text
    visibilitylkmt = int(visibilitylkmt)
    rules(windir04,windspeed04,visibilitylkmt,lkmt,rwy04,rwy22,ga9,ga10,ga11,ga12)


def rules(winddir,windspeed1,visibility,airport,runway1,runway2,godangle1,godangle2,godangle3,godangle4):
    while True:
        air = colored(200,250,0, airport)
        print(air)
        if visibility > 2000:
            x = "VISIBILITY IS GREATER THAN 2000M, USE VISUAL."
            x = colored(0,255,0, x)
            print(x)
        else:
            y = "VISIBILITY IS LOW, USE ILS APPROACH."
            y = colored(255,0,0, y)
            print(y)
        if winddir < godangle1 and winddir > godangle2 and windspeed1 < 40:
            runwaya = "USE: ",runway1,"WIND DIRECTION: ",winddir,"°","WIND SPEED: ",windspeed1," KNOTS","VISIBILITY: ",visibility," METERS"
            print(runwaya[0] + runwaya[1])
            print(runwaya[2] + str(runwaya[3]) + runwaya[4])
            print(runwaya[5] + str(runwaya[6]) + runwaya[7])
            print(runwaya[8] + str(runwaya[9]) + runwaya[10])
        elif winddir < godangle3 and winddir > godangle4 and windspeed1 < 40:
            runwayb = "USE: ",runway2,"WIND DIRECTION: ",winddir,"°","WIND SPEED: ",windspeed1," KNOTS","VISIBILITY: ",visibility," METERS"
            print(runwayb[0] + runwayb[1])
            print(runwayb[2] + str(runwayb[3]) + runwayb[4])
            print(runwayb[5] + str(runwayb[6]) + runwayb[7])
            print(runwayb[8] + str(runwayb[9]) + runwayb[10])
        else:
            goaround = ("LANDING ON RUNWAYS IS NOT POSSIBLE, GO AROUND!")
            goaround = colored(255,0,0, goaround)
            print(goaround)
        break


def main():
    x = 1
    intro = "BEST POSSIBLE APPROACH V.1.0"
    print(colored(150,200,246, intro))
    while x == 1:
        lkpr()
        time.sleep(10)
        cls()
        lkkv()
        time.sleep(10)
        cls()
        lkmt()
        time.sleep(10)
        cls()


if __name__ == "__main__":
    sys.exit(main())

