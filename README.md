# Best-Possible-Approach:

This program selects the best possible approach on three different runways located in the Czech Republic in a cycle of thirty seconds. The program uses online data from the Meteorological Aerodrome Report. 


<img src="./papp.png">

# Technology:

- Time
- Selenium


# RULES: 
- A. All.ways prefer headwind against tailwind! 
     


# Install:

- extract all files to new directory
- open console and go to directory
- enter ./install.sh


# Uninstall:

- open console and go to directory
- enter ./uninstall.sh


# How to Run it:

- open console
- enter [username@fedora ~]$ papp


# License:

- opensource
